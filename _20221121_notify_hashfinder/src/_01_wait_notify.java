import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class _01_wait_notify {
    public static void main(String[] args) {

        DataShareWait ds = new DataShareWait();

        Runnable sender = () -> {
            for (int i = 0; i < 1000; i++) {
                ds.setDataString("Message: " + Integer.toString(i));

                // try {
                // Thread.sleep(1000);
                // } catch (InterruptedException e) {
                // e.printStackTrace();
                // }
            }
        };

        Runnable receiver = () -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("Got Message: " + ds.getDataString());
            }
        };

        new Thread(sender).start();
        new Thread(receiver).start();
    }
}

class DataShareWait {

    private String dataString;
    BlockingQueue<String> queue = new ArrayBlockingQueue<>(1);

    synchronized public String getDataString() {
        try {
            queue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return dataString;
    }

    synchronized public void setDataString(String message) {

        try {
            queue.put(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.dataString = message;
        System.out.println("Message sent!");

    }
}
