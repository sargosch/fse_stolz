import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class _04_hashfinder_new {
    public static void main(String[] args) {

        String data = "Sarah";
        Random random = new Random();

        String difficulty = "000";

        HashFinder1.found.set(false);

        long startTime;
        double durchschnitt = 0;
        double durchschnitt1 = 0;
        double durchschnitt2 = 0;

        int howManyThreads1 = 1;
        int howManyThreads2 = 4;
        int howManyThreads3 = 6;

        int loop = 100;

        for(int i = 0; i < loop; ++i) {
            String transactionData = data + random.nextInt(10000, Integer.MAX_VALUE);

            startTime = System.currentTimeMillis();

            ThreadManager.startThatThreads(howManyThreads1, transactionData, difficulty);

            ThreadManager.waitForTrue();

            durchschnitt += System.currentTimeMillis() - startTime;

            HashFinder1.found.set(false);

            startTime = System.currentTimeMillis();

            ThreadManager.startThatThreads(howManyThreads2, transactionData, difficulty);

            ThreadManager.waitForTrue();

            durchschnitt1 += System.currentTimeMillis() - startTime;

            HashFinder1.found.set(false);

            startTime = System.currentTimeMillis();

            ThreadManager.startThatThreads(howManyThreads3, transactionData, difficulty);

            ThreadManager.waitForTrue();

            durchschnitt2 += System.currentTimeMillis() - startTime;

            HashFinder1.found.set(false);
        }

        System.out.printf("Durchschnittszeit bei der Suche mit %d Threads: %3.2f ms %n", howManyThreads1, (durchschnitt/loop));
        System.out.printf("Durchschnittszeit bei der Suche mit %d Threads: %3.2f ms %n", howManyThreads2, (durchschnitt1/loop));
        System.out.printf("Durchschnittszeit bei der Suche mit %d Threads: %3.2f ms %n", howManyThreads3, (durchschnitt2/loop));

    }

}

class ThreadManager {

    public static void startThatThreads(int howManyThreads, String transactionData, String difficulty) {
        for (int k = 0; k < howManyThreads; ++k) {
            new Thread(new Runner(transactionData, difficulty, (Long.MAX_VALUE/ howManyThreads)*k)).start();
        }
    }

    public static void waitForTrue() {
        while (!HashFinder1.found.get()) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

class Runner implements Runnable {

    String transactionData;
    String difficulty;
    long startNonce;

    public Runner(String transactionData, String difficulty, long startNonce) {
        this.transactionData = transactionData;
        this.difficulty = difficulty;
        this.startNonce = startNonce;
    }

    @Override
    public void run() {
        while(!HashFinder1.found.get()) {
            HashFinder1.findHash(transactionData, difficulty, startNonce);
        }
    }
}

class HashFinder1 {

    static AtomicBoolean found = new AtomicBoolean();

    public static boolean findHash(String transactionData, String difficutly, long startNonce) {

        while (!found.get() && startNonce < Long.MAX_VALUE) {
            String strToHash = transactionData + startNonce;
            String hash = calculateHash(strToHash);
            // System.out.printf("Try with nonce %d from Thread %s - %s %n", nonce, Thread.currentThread().getName(), hash);
            if (hash.startsWith(difficutly)) {
                found.set(true);
                //System.out.printf("Found with nonce %d from Thread %s - %s %n", startNonce, Thread.currentThread().getName(), hash);

            }
            ++startNonce;
        }
        return found.get();
    }

    /**
     * Calculates sha256 to a String
     *
     * @param strToHash
     * @return
     */
    public static String calculateHash(String strToHash) {

        byte[] bytesOfMessage;
        try {
            bytesOfMessage = strToHash.getBytes("UTF-8");
            MessageDigest md;
            md = MessageDigest.getInstance("SHA-256");

            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                String hex = Integer.toHexString(0xff & thedigest[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}





