import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class _03_hashfinder {
    public static void main(String[] args) {

        String transactionData = "Hallo Sarah"; //zufall
        String difficulty = "0000";
        int howManyThreads = 4;

        Runnable runner = () -> {
            //HashFinder1.findHash(transactionData, difficulty);
            while(!HashFinder.isFound) {
                    HashFinder.findHash(transactionData, difficulty);
            }
            Thread.currentThread().interrupt();
        };

        for (int i = 0; i < howManyThreads; ++i) {
            new Thread(runner).start();
        }

    }

}

class HashFinder {

    static boolean isFound; //AtomicBoolean

    public static boolean findHash(String transactionData, String difficutly) {

        isFound = false;
        //Thread abfragen
        while (!isFound && Storage.getNonce() < Long.MAX_VALUE) {
            long nonce = Storage.getNonceAndCountUp();
            String strToHash = transactionData + nonce;
            String hash = calculateHash(strToHash);
            // System.out.printf("Try with nonce %d from Thread %s - %s %n", nonce, Thread.currentThread().getName(), hash);
            if (hash.startsWith(difficutly)) {
                isFound = true;
                System.out.printf("Found with nonce %d from Thread %s - %s %n", nonce, Thread.currentThread().getName(), hash);

            }
        }
        return isFound;
    }

    /**
     * Calculates sha256 to a String
     *
     * @param strToHash
     * @return
     */
    public static String calculateHash(String strToHash) {

        byte[] bytesOfMessage;
        try {
            bytesOfMessage = strToHash.getBytes("UTF-8");
            MessageDigest md;
            md = MessageDigest.getInstance("SHA-256");

            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                String hex = Integer.toHexString(0xff & thedigest[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

    }

}
//nonce in Bereiche
class Storage {
    private static long nonce;

    synchronized public static long getNonce() {
        return nonce;
    }

    synchronized public static long getNonceAndCountUp() {
        long actualNonce = nonce;
        nonce++;
        return actualNonce;
    }

}



