public class _02_runnable {

    // TODO Lambda Expressions wiederholen
    public static void main(String[] args) {
        

        int howMany = 10;
        
        for(int i = 1; i <= howMany; ++i) {
            
            String name = "Thread " + i;
            (new Thread(new Job(10), name )).start();
            
        }
        // TODO füge eine Schleife hinzu, die so viele Threads startet, wie angegeben

    }
}

class Job implements Runnable {

    int runs;

    public Job(int runs){
        this.runs = runs;
    }

    @Override
    public void run() {

        for (int i = 0; i < runs; i++) {

            String threadName = Thread.currentThread().getName();
            System.out.printf("%s - %d %n",threadName,i);
            // Wenn sleep() weggelassen wird, finden die Ausgaben konkurrierender statt
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }


}
